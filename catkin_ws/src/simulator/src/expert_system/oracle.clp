;************************************************
;*						*
;*	oracle.clp 				*
;*						*
;*	Jesus Savage				*
;*						*
;*		Bio-Robotics Laboratory		*
;*		UNAM, 2019			*
;*						*
;*						*
;************************************************


(defrule clips-alive
	?f <- (alive clips)
	=>
	(retract ?f)
	(printout t "ROS clips alive ROS")
)


(defrule Max-Values
	(max-advance ?max-advance max-rotation ?max-rotation)
	=>
	(printout t "ROS received max advance rotation ROS")
)


(defrule obs-dest
        ?f <- (step ?num intensity ?int obs ?obs dest ?dest)
        =>
        (retract ?f)
        ;?num indice de lectura en sensores
        ;?obs lectura sensores proximidad
        ;?dest lectura sensores de luz
	(assert (received ?num ?obs ?dest))
        ;?int maxima intensidad de luz
	(assert (intensity ?num ?int))
	(bind ?num (* ?num 1))
        ;(printout t "Modified number " ?num)
)



; comportamientos para salir de BUG
; girar hacia la luz
(defrule no-obstacle-light-not-front
        ;received ?num sensor_proximidad sensor_luz
        ;0 ~0 no obstaculo y luz no enfrente
        ?f <- (received ?num 0 ?light&~0)
	(max-advance ?max-advance max-rotation ?max-rotation)
        (not(choque))																												;//<------------------Negar "bandera" para evitar circulito
        =>
        (retract ?f)
        (if (> ?light 4) then
                (bind ?rot (* 0.7853 (- ?light 8)))
	else
                (bind ?rot (* 0.7853 ?light))
        )
        ;accion: ir hacia adelante
        (assert (movement ?num ?rot 0 0.5))
)
; avanzar hacia la luz si no ha chocado con obstaculo
(defrule no-obstacle-light-front
        ;received ?num sensor_proximidad sensor_luz
        ;0 0 no obstaculo y luz enfrente
        ?f <- (received ?num 0 0)
	(max-advance ?max-advance max-rotation ?max-rotation)
        (not (choque))
        =>
        (retract ?f)
	(bind ?forward ?max-advance)
        ;accion: ir hacia adelante
        (assert (movement ?num 0 ?forward 0.5))
)

;si choca con obstaculo marcar choque y girar a la izquierda
(defrule obstacle-front-light-front
        ;3 0 obstaculo enfrente y luz enfrente
        ?f <- (received ?num 3 0)
	(max-advance ?max-advance max-rotation ?max-rotation)
	(not(afterbug))																															;//<------------------Negar "bandera" para evitar circulito
        =>
        (retract ?f)
	(bind ?forward ?max-advance)
        ;accion: girar 90 a izquierda y avanzar
        (assert (movement ?num 1.5707 ?forward 0.5))
        (assert (choque))
)
;si choca con obstaculo marcar choque y girar a la izquierda
(defrule second-obstacle-front-light-front
        ;3 0 obstaculo enfrente y luz enfrente
        ?f <- (received ?num 3 0)
	(max-advance ?max-advance max-rotation ?max-rotation)
	?a <- (afterbug)
        (choque)																															;//<------------------Negar "bandera" para evitar circulito
        =>
        (retract ?f)
        (retract ?a)
	(bind ?forward ?max-advance)
        ;accion: girar 90 a izquierda y avanzar
        (assert (movement ?num 1.5707 ?forward 0.5))
        
)
;si choca enfrente y ya habia chocado girar a la izquierda
(defrule obstacle-front-light-anywhere
        ;obstaculo enfrente no importa luz
        ?f <- (received ?num 3 ?light)
        ;choque previo
        (choque)
	;(not(afterbug))
	(max-advance ?max-advance max-rotation ?max-rotation)
        =>
        (retract ?f)
	(bind ?forward ?max-advance)
        ;accion: girar 90 a izquierda y avanzar
        (assert (movement ?num 1.5707 ?forward 0.5))

)

;---notas no mentales---
;Light frente 0
;Light frente izquierda 1
;Light izquierda 2
;Light atras izquierda 3
;Light atras 4
;			....

;previo choque obstaculo a la derecha
;obstaculo/pared a la derecha 1
;luz no a la izquierda 2
(defrule obstacle-right-light-anywhere
        ?f <- (received ?num 1 ?light&~2)												;//<------------------------------------------
        ;choque previo
        (choque)
				;(not(afterbug))
	(max-advance ?max-advance max-rotation ?max-rotation)
        =>
        (retract ?f)
	(bind ?forward ?max-advance)
        ;accion:  avanzar
        (assert (movement ?num 0 ?forward 0.5))

)

; si no tiene obstaculo a la derecha y habia chocado gira a la derecha y avanzar //<---------------EL GIRO SE REDUJO A 45 PARA PERMITIR EL GIRO EN LAS ESQUINAS
(defrule no-obstacle-light-anywhere
        ;0 no obstaculo
        ?f <- (received ?num 0 ?light )
        ;choque previo
				(not(afterbug))
        (choque)
	(max-advance ?max-advance max-rotation ?max-rotation)
        =>
        (retract ?f)
	(bind ?forward ?max-advance)
        (bind ?rot (- 0 0.7853))
        ;accion: gira 45 izquierda y avanzar
        (assert (movement ?num ?rot ?forward 0.5))
)

; si sensor izquierdo marca luz girar hacia luz //<---------------------NO SE QUITA LA BANDERA CHOQUE, SE AGREGA BANDERA AFTERBUG
(defrule obstacle-right-light-front-left
        ;luz a la izquierda
        ;obstaculo/pared a la derecha
        ?f <- (received ?num 1 2)
	(max-advance ?max-advance max-rotation ?max-rotation)
        ;?c<=(choque)
        =>
        (retract ?f)
        ;(retract ?c)
	(bind ?forward ?max-advance)
        ;accion: girar a luz y avanzar
        (assert (movement ?num 1.5707 ?forward 0.5))
	;nueva "bandera" indica que saliió del mapa bug
	(assert (afterbug))
)

(defrule afterbug-light-front-right																		;//<----------------NO OBSTACULO/LUZ ENFRENTE A LA DERECHA --- DESPUES DEL MAPA
        ;received ?num sensor_proximidad sensor_luz
        ;0 0 no obstaculo y luz enfrente derecha
        ?f <- (received ?num ?obs ?light)
	(choque)
	(afterbug)
	(max-advance ?max-advance max-rotation ?max-rotation)
        =>
        (retract ?f)
	(bind ?forward ?max-advance)
        (if (> ?light 4) then
                (bind ?rot (* 0.7853 (- ?light 8)))
	else
                (bind ?rot (* 0.7853 ?light))
        )
        ;accion: ir hacia adelante
        (assert (movement ?num ?rot ?forward 0.5))
)


(defrule arbiter
	?f <- (movement ?num ?rotation ?advance ?status)
	?f1 <- (intensity ?num ?intensity)
	=>
	(retract ?f ?f1)
	(if (> ?intensity 30.0) then
		(printout t "ROS movement " ?num " " ?rotation " " ?advance " " 1.0 " ROS")
	else
		(printout t "ROS movement " ?num " " ?rotation " " ?advance " " ?status " ROS")
	)
)


(defrule delete-unused-intensities
	(declare (salience -1000))
        ?f <- (intensity ?num $?)
        =>
        (retract ?f)
)

(defrule delete-unused-received
	(declare (salience -1000))
        ?f <- (received ?num $?)
        =>
        (assert (movement ?num 0.0 0.0 0.5))
        (retract ?f)
)
